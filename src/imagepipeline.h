/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef IMAGEPIPELINE_H
#define IMAGEPIPELINE_H

#include <QObject>
#include <vector>

#include <QFutureWatcher>

#include "profile.h"
#include "cameras.h"

class ImagePipeline: public QObject
{
	Q_OBJECT

private:

	Cameras* cameras_;
	Profile profile_;
	bool invalid_ = true;
	std::vector<QFutureWatcher<cv::Mat>*> futureImageWatchers_;
	bool simpleStichingAlg_;

	static cv::Mat process(const Profile profile, std::vector<Camera::Image> images, bool simpleStich);
	static void applyDarkMap(cv::Mat& image, const cv::Mat &darkmap);
	static void sanityCheckMap(cv::Mat& mat);

private slots:

	void apply(std::vector<Camera::Image> images);
	void imageFinished();

signals:
	void sigInvalidProfile(QString message);
	void sigResult(Camera::Image image);
	void statusMsg(QString msg);

public slots:
	void setProfile(const Profile& profile);

public:
	ImagePipeline(Cameras* cameras, bool simpleStichingAlg = false, QObject* parent = nullptr);
};

#endif // IMAGEPIPELINE_H

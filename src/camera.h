/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include <vector>
#include <uvoscam.h>
#include <uvosled.h>
#include <memory.h>
#include <stdint.h>
#include <QSettings>
#include <opencv2/core/mat.hpp>

#include "profile.h"

class Camera : public QObject
{
	Q_OBJECT
public:
	class Image
	{
	public:
		cv::Mat mat;
		uint64_t cameraId;
		Image(cv::Mat mati, uint64_t cameraIdi): mat(mati), cameraId(cameraIdi){}
		Image() = default;
		Image(const Image& img) = default;
	};
private:
	cam::Camera* camera_ = nullptr;

	void callback(cv::Mat);
	uint64_t cameraId_;

signals:
	void newImage(Image);

public:
	Camera(cam::Camera::Description);
	~Camera();
	cam::Camera* cam(){return camera_;}
	uint64_t id() const {return cameraId_;}
	bool operator==(const Camera& cam){return cam.id() == id();}
	bool operator!=(const Camera& cam){return !operator==(cam);}
};

#endif // CAMERA_H

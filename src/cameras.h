/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CAMERAS_H
#define CAMERAS_H

#include <QObject>
#include <vector>
#include <uvoscam.h>
#include <uvosled.h>
#include <memory.h>
#include <stdint.h>
#include <QSettings>
#include <opencv2/core/mat.hpp>
#include <QTimer>

#include "profile.h"
#include "camera.h"

class Cameras : public QObject
{
	Q_OBJECT

	static constexpr const char* GROUP = "Cameras";

private:

	uvosled* led_;
	bool free_ = false;
	bool disable_ = false;
	double exposrueTime_ = 1.0/60.0;
	uint64_t blockCaptureId_ = 0;
	LightingSetup lighting_;
	std::vector<std::shared_ptr<Camera>> cameras_;
	std::vector<Camera::Image> images_;
	size_t captureingCamera = 0;
	QTimer ledTimer;
	QTimer cameraFailureTimer;
	bool quirks_;
	bool serial_;

	bool lightFor(const LightingSetup& lighting, double time);

private slots:
	void imageRecived(Camera::Image img);
	void finishAddCamera(std::shared_ptr<Camera> camera);
	void lightOff();

signals:
	void cameraRemoved(std::shared_ptr<Camera> camera);
	void cameraAdded(std::shared_ptr<Camera> camera);
	void newImages(std::vector<Camera::Image> images);
	void enableCapture(bool enable);

public slots:

	bool setExposureTime(double exposureTime);
	void trigger();
	bool start();
	bool stop();
	bool setFree(bool free);
	void setLighting(const LightingSetup& lighting) {lighting_ = lighting;}
	void reloadCameras();

public:
	Cameras(uvosled* led = nullptr, bool quirks = true, bool serial = false);
	~Cameras();
	bool setCameras(const std::vector<cam::Camera::Description>& descriptions);
	bool addCamera(const cam::Camera::Description& desc);
	void setSetup(const std::vector<CameraSetup>& setups);
	std::vector<cam::Camera::Description> getCameras();
	std::shared_ptr<Camera> getCamera(size_t id);
	size_t numCameras(){return cameras_.size();}
	void clear();
	double getMeanTemp();
	void load(QSettings& settings);
	void store(QSettings& settings);
	void disable(bool disable);

	inline static float quirkTime = 3;
};

#endif // CAMERAS_H

/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "camera.h"
#include <QRandomGenerator>
#include <string>
#include <functional>
#include <QDebug>

Camera::Camera(cam::Camera::Description desc)
{
	cameraId_ = desc.getId();
	std::function<void(cv::Mat)> cb = [this](cv::Mat image){newImage(Image(image, cameraId_));};
	camera_ = new cam::Camera(cb);
	camera_->openCamera(desc);
}

Camera::~Camera()
{
	delete camera_;
}

/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PROFILEDIALOG_H
#define PROFILEDIALOG_H

#include <QDialog>
#include "../cameras.h"

namespace Ui {
class ProfileDialog;
}

class ProfileDialog : public QDialog
{
	Q_OBJECT

	Cameras* cameras_;

private slots:
	void addProfile();
	void editProfile();
	void deleteProfile();

public:
	explicit ProfileDialog(Cameras* cameras, QWidget *parent = nullptr);
	~ProfileDialog();

private:
	Ui::ProfileDialog *ui;
};

#endif // PROFILEDIALOG_H

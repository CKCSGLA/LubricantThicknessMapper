/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef LED_H
#define LED_H

#include <QWidget>

class Led : public QWidget
{
	Q_OBJECT

	bool lit_ = false;

	public:

	Led(QWidget* parent = nullptr);
	bool lit() const;

	public slots:

	void setLit(bool lit);

	signals:

	void stateChanged(bool lit);
	protected:
	virtual void paintEvent(QPaintEvent* event) override;


};
#endif // LED_H

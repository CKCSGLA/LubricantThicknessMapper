/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "regressiondiag.h"
#include "ui_regressiondiag.h"

RegressionDiag::RegressionDiag(const RegessionCalculator& reg, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegressionDiag)
{
    ui->setupUi(this);

    ui->lcdNumber_slope->display(reg.slope);
    ui->lcdNumber_offset->display(reg.offset);
    ui->lcdNumber_StdError->display(reg.stdError);
    ui->lcdNumbe_count->display((int)reg.xValues.size());

#ifdef Q_OS_ANDROID
    setWindowState(Qt::WindowMaximized);
#endif
}

RegressionDiag::~RegressionDiag()
{
    delete ui;
}


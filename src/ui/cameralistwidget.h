/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CAMERALISTWIDGET_H
#define CAMERALISTWIDGET_H

#include <vector>
#include <uvoscam.h>
#include <QTableWidget>

class CameraListWidget : public QTableWidget
{
private:
	std::vector<cam::Camera::Description> desc_;
public:
	CameraListWidget(QWidget* parent);
	void setCameras(const std::vector<cam::Camera::Description>& desc);
	void setConfigured(std::vector<bool> configured);
	std::vector<cam::Camera::Description> getSelectedDescriptions();
};

#endif // CAMERALISTWIDGET_H

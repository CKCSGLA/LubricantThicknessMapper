/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CAMERADIALOG_H
#define CAMERADIALOG_H

#include <QDialog>
#include <vector>
#include <uvoscam.h>
#include <QSettings>

namespace Ui {
class CameraDialog;
}

class CameraDialog : public QDialog
{
	Q_OBJECT

public:
	explicit CameraDialog(const std::vector<cam::Camera::Description>& desc, QWidget *parent = nullptr);
	~CameraDialog();
	std::vector<cam::Camera::Description> getDescriptions();

private:
	Ui::CameraDialog *ui;
};

#endif // CAMERADIALOG_H

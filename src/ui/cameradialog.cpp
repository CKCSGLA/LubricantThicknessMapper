/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "cameradialog.h"
#include "ui_cameradialog.h"
#include "../cameras.h"

CameraDialog::CameraDialog(const std::vector<cam::Camera::Description>& desc, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::CameraDialog)
{
	ui->setupUi(this);
	ui->listView->setCameras(desc);
}

CameraDialog::~CameraDialog()
{
	delete ui;
}

std::vector<cam::Camera::Description> CameraDialog::getDescriptions()
{
	return ui->listView->getSelectedDescriptions();
}

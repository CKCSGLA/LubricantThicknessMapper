/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef EDDITPROFILEDIALOG_H
#define EDDITPROFILEDIALOG_H

#include <QDialog>
#include <profile.h>
#include "../cameras.h"

namespace Ui {
class EditProfileDialog;
}

class EditProfileDialog : public QDialog
{
	Q_OBJECT
	Profile profile_;
	Cameras* cameras_;

	bool setConfigured();
	void invalidateCameras();

private slots:
	void setMask();
	void configureCamera();
	void loadCalcurve();
	void loadLightmap();

public slots:
	virtual void accept() override;

public:
	explicit EditProfileDialog(Cameras* cameras, const Profile profile = Profile(), QWidget *parent = nullptr);
	Profile getProfile(){return profile_;}
	~EditProfileDialog();

private:
	Ui::EditProfileDialog *ui;
};

#endif // EDDITPROFILEDIALOG_H

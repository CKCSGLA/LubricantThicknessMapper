/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef STATISTICSDIALOG_H
#define STATISTICSDIALOG_H

#include <QDialog>
#include <vector>

namespace Ui {
class StatisticsDialog;
}

class StatisticsDialog : public QDialog
{
	Q_OBJECT

public:
	explicit StatisticsDialog(const std::vector<double>& data, QWidget *parent = 0);
	~StatisticsDialog();

private slots:
	void copyCount();
	void copyMean();
	void copyMinMax();
	void copySd();

private:
	Ui::StatisticsDialog *ui;
};

#endif // STATISTICSDIALOG_H

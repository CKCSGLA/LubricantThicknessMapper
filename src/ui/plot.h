/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PLOT_H
#define PLOT_H

#include<QVector>
#include<QAction>
#include<QWidget>

#include "qcustomplot/qcustomplot.h"
#include "regessioncalculator.h"
#include "statisticsdialog.h"
#include "utilites.h"

class Plot : public QCustomPlot
{
private:
	std::vector<RegessionCalculator> regressions;

	QMenu graphContextMenu;

	int graphPointLimit = 10000;
	QAction actionStatistics;
	QAction actionAdd_Regression;
	QAction actionDelete_Regression;
	QAction actionExport_Selection;
	QAction actionSetValueString;
	QAction savePdfAction;

private slots:
	void savePdf();

public:
	Plot(QWidget* parent = nullptr);
	~Plot();

	void setLabel(QString label);

	void clear();
	void setLimit(int graphPointLimit);
	int getLimit();

	void setMaxValue(double maxVal);

signals:
	void sigSaveCsv();

	public slots:
	void addPoints(QVector<double> keys, QVector<double> values);
	void saveCsv(QString fileName);
	void saveCsvDiag();
	void showStatistics();
	void deleteRegression();
	void addRegression();
	void askForValueString();

	void addData(QVector<double> keys, QVector<double> values, bool inOrder = false, bool ignoreLimit = false);
	void addData(double key, double value, bool ignoreLimit = false);

protected:
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	virtual bool event(QEvent *event);

private:
	void addMainGraph();
};

#endif // PLOT_H


/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "led.h"
#include <QPainter>

Led::Led(QWidget* parent): QWidget(parent)
{
	setMinimumSize(QSize(40,40));
	setSizePolicy(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Fixed);
}

bool Led::lit() const
{
	return lit_;
}

void Led::setLit(bool lit)
{
	if(lit != lit_)
	{
		lit_ = lit;
		stateChanged(lit_);
		update();
	}
}

void Led::paintEvent(QPaintEvent* event)
{
	Q_UNUSED(event)
	QPainter ledPainter(this);
	ledPainter.setRenderHint(QPainter::Antialiasing, true);
	ledPainter.setPen(Qt::black);
	if(lit_)
		ledPainter.setBrush(Qt::green);
	else
		ledPainter.setBrush(Qt::red);
	ledPainter.drawEllipse(rect().adjusted(0, 0, -1, -1));
}

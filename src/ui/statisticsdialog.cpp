/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "statisticsdialog.h"
#include "ui_statisticsdialog.h"

#include <QClipboard>
#include <limits>
#include <cmath>

StatisticsDialog::StatisticsDialog(const std::vector<double>& data, QWidget *parent):
	QDialog(parent),
	ui(new Ui::StatisticsDialog)
{
	ui->setupUi(this);

	connect(ui->pushButton_1, &QPushButton::clicked, this, &StatisticsDialog::copyCount);
	connect(ui->pushButton_4, &QPushButton::clicked, this, &StatisticsDialog::copyMean);
	connect(ui->pushButton_5, &QPushButton::clicked, this, &StatisticsDialog::copyMinMax);
	connect(ui->pushButton_Sd, &QPushButton::clicked, this, &StatisticsDialog::copySd);

	ui->lcdNumber_Count->display(static_cast<double>(data.size()));

	double mean = 0;
	double max = std::numeric_limits<double>::min();
	double min = std::numeric_limits<double>::max();
	for(double point : data)
	{
		mean += point;
		if(point < min)
			min = point;
		if(point > max)
			max = point;
	}
	mean = mean / data.size();
	ui->lcdNumber_Mean->display(mean);

	double sd = 0;
	for(double point : data)
	{
		sd += (point - mean)*(point - mean);
	}
	sd /= data.size();
	sd = sqrt(sd);
	ui->lcdNumber_Sd->display(sd);

	ui->lcdNumber_Max->display(max);
	ui->lcdNumber_Min->display(min);
}

void StatisticsDialog::copyCount()
{
	QString buffer(QString::number(ui->lcdNumber_Count->value(), 'g', 10));
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Clipboard);
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Selection);
}

void StatisticsDialog::copyMean()
{
	QString buffer(QString::number(ui->lcdNumber_Mean->value(), 'g', 10));
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Clipboard);
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Selection);
}

void StatisticsDialog::copyMinMax()
{
	QString buffer(QString::number(ui->lcdNumber_Min->value(), 'g', 10));
	buffer.append(",");
	buffer.append(QString::number(ui->lcdNumber_Max->value(), 'g', 10));
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Clipboard);
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Selection);
}

void StatisticsDialog::copySd()
{
	QString buffer(QString::number(ui->lcdNumber_Sd->value(), 'g', 10));
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Clipboard);
	QGuiApplication::clipboard()->setText(buffer, QClipboard::Selection);
}

StatisticsDialog::~StatisticsDialog()
{
	delete ui;
}


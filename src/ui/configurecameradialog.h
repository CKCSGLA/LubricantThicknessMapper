/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CONFIGURECAMERADIALOG_H
#define CONFIGURECAMERADIALOG_H

#include <QDialog>
#include "cameras.h"
#include "profile.h"

namespace Ui {
class ConfigureCameraDialog;
}

class ConfigureCameraDialog : public QDialog
{
	Q_OBJECT

	static constexpr int MODE_IDLE = 0;
	static constexpr int MODE_REMAP_GET = 1;
	static constexpr int MODE_BG_GET = 2;
	static constexpr int MODE_DARK_GET = 3;

	CameraSetup setup_;
	std::shared_ptr<Camera> camera_;
	int mode_ = MODE_IDLE;
	cv::Mat fgImage;
	double profileExposure_;
	bool nodistort_;

private:
	bool checkConfig();
	void gotImage(Camera::Image img);

private slots:
	void captureBg();
	void captureRemap();
	void captureDark();
	void takeImage();
	void setExposure(double value);
	void bayerIndexChanged(int index);
	void setGain(double value);

public slots:
	void accept() override;

public:
	explicit ConfigureCameraDialog(const CameraSetup& setup, const  std::shared_ptr<Camera> camera, double exposureTime = 1.0/60, bool nodistort = false, QWidget *parent = nullptr);
	~ConfigureCameraDialog();
	CameraSetup getCameraSetup(){return setup_;}


private:
	Ui::ConfigureCameraDialog *ui;
};

#endif // CONFIGURECAMERADIALOG_H

/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <stdint.h>
#include "../cameras.h"
#include "cvimageviewer.h"
#include "aboutdiag.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT
private:
	std::vector<CvImageViewer*> viewers_;
	AboutDiag about_;
	inline static QString lastSavedPath_ = "./";

private slots:
	void setImageValue(size_t x, size_t y, double value);
	void saveImage();
	void openImage();

signals:
	void sigCapture();
	void sigProfile(QString profileName);
	void sigChooseCameras();
	void sigEditProfiles();

public slots:

	void addCamera(std::shared_ptr<Camera> camera);
	void refreshProfiles();
	void profileInconpatible(QString message);
	void removeCamera(std::shared_ptr<Camera> camera);
	bool setProfile(const QString& profileName);
	QString getProfileName();
	void statusMsg(QString msg);
	void enableCapture(bool capture);
	void setTemperature(double temp);

public:
	MainWindow(bool viewer = false, QWidget *parent = nullptr);
	~MainWindow();
	CvImageViewer* mainImageViewer();

private:
	Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H

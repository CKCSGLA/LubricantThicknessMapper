/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef REGRESSIONDIAG_H
#define REGRESSIONDIAG_H

#include <QDialog>

#include "regessioncalculator.h"

namespace Ui {
class RegressionDiag;
}

class RegressionDiag : public QDialog
{
    Q_OBJECT

public:
    explicit RegressionDiag(const RegessionCalculator& reg, QWidget *parent = 0);
    ~RegressionDiag();

private:
    Ui::RegressionDiag *ui;
};

#endif // REGRESSIONDIAG_H

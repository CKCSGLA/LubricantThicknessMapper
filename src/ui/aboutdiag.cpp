/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "aboutdiag.h"
#include "ui_aboutdiag.h"
#include <QPalette>

AboutDiag::AboutDiag(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AboutDiag)
{
	ui->setupUi(this);
	setFixedSize(size());

	QPalette pal = palette();
	pal.setColor(QPalette::Window, QColor(52,52,60));
	pal.setColor(QPalette::WindowText, QColor(255,255,255));
	pal.setColor(QPalette::Text, QColor(255,255,255));
	setAutoFillBackground(true);
	setPalette(pal);
}

AboutDiag::~AboutDiag()
{
	delete ui;
}

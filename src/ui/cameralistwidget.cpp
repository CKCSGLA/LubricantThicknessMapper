/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "cameralistwidget.h"
#include <QDebug>
#include <QHeaderView>
#include "../cameras.h"

CameraListWidget::CameraListWidget(QWidget* parent): QTableWidget(parent)
{
	setColumnCount(1);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setSelectionMode(QAbstractItemView::MultiSelection);
	setHorizontalHeaderItem(0, new QTableWidgetItem("Camera"));
	setRowCount(desc_.size());
	horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
	setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void CameraListWidget::setConfigured(std::vector<bool> configured)
{
	setColumnCount(2);
	setHorizontalHeaderItem(1, new QTableWidgetItem("Configured"));
	for(size_t i = 0; i < desc_.size() && i < configured.size(); ++i)
	{
		setItem(static_cast<int>(i), 1, new QTableWidgetItem(configured[i] ? "Yes" : "No"));
		qDebug()<<"Set item "<<i<<1;
	}
}

void CameraListWidget::setCameras(const std::vector<cam::Camera::Description>& desc)
{
	desc_ = desc;
	qDebug()<<"cameras: "<<desc_.size();
	setRowCount(static_cast<int>(desc_.size()));
	for(size_t i = 0; i < desc_.size(); ++i)
		setItem(static_cast<int>(i), 0, new QTableWidgetItem((desc_[i].getVendor() + " " + desc_[i].getModel()).c_str()));
}

std::vector<cam::Camera::Description> CameraListWidget::getSelectedDescriptions()
{
	 QList<QModelIndex> selected = selectedIndexes();
	std::vector<cam::Camera::Description> cameras;
	for(auto& selection : selected)
	{
		if(selection.column() == 0)
			cameras.push_back(desc_[selection.row()]);
	}
	return cameras;
}

/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CVIMAGEVIEWER_H
#define CVIMAGEVIEWER_H

#include <QWidget>
#include <QPainter>
#include <QMenu>
#include <QSlider>
#include <limits>
#include "../cameras.h"
#include "plot.h"

class CvImageViewer : public QWidget
{
	Q_OBJECT

private:
	cv::Mat origImage_;
	cv::Mat image_;
	QImage qimage_;
	bool fixedOnWidth_ = false;
	size_t lastId_;
	QMenu imageContextMenu_;
	Plot plot;
	QAction saveAction_;
	QAction exportAction_;
	QAction zoomAction_;
	QAction resetAction_;
	QAction statisticsAction_;
	QAction xPlotAction_;
	QAction yPlotAction_;
	QRect imgrect_;
	cv::Rect roi_;
	QRect selectionRect_;
	QLine xLine_;
	QLine yLine_;
	bool selectionStarted_ = false;
	double clamp_ = std::numeric_limits<double>::max();
	inline static QString lastSavePath_ = "./";

	void transfromToSourceCoordinates(int inX, int inY, int& outX, int& outY);
	void convertImage(cv::Mat image);
	cv::Rect roiFromSelection();

private slots:
	void saveImage();
	void exportImage();
	void zoomToSelection();
	void resetZoom();
	void showSatDiag();
	void plotOnX();
	void plotOnY();

protected:
	virtual void paintEvent(QPaintEvent* event) override;
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void leaveEvent(QEvent *event) override;

signals:
	void sigValue(size_t x, size_t y, double value);
	void sigMax(double max);

public slots:
	void setImage(Camera::Image img);
	void setClamp(double max);

public:
	explicit CvImageViewer(QWidget *parent = nullptr, size_t lastId = 0);
	void setFixedOnWidth(bool in){fixedOnWidth_ = in;}
	cv::Mat getImage(){return origImage_;}

	size_t lastId(){return lastId_;}
	~CvImageViewer();
};

#endif // CVIMAGEVIEWER_H


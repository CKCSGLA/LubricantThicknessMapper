/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "profiledialog.h"
#include "ui_profiledialog.h"
#include "editprofiledialog.h"

#include <QMessageBox>

ProfileDialog::ProfileDialog(Cameras* cameras, QWidget *parent) :
	QDialog(parent),
	cameras_(cameras),
	ui(new Ui::ProfileDialog)
{
	ui->setupUi(this);
	ui->listWidget->addItems(Profile::avaiableProfiles());
	connect(ui->pushButtonAdd, &QPushButton::clicked, this, &ProfileDialog::addProfile);
	connect(ui->pushButtonEdit, &QPushButton::clicked, this, &ProfileDialog::editProfile);
	connect(ui->pushButtonDelete, &QPushButton::clicked, this, &ProfileDialog::deleteProfile);
}


ProfileDialog::~ProfileDialog()
{
	delete ui;
}

void ProfileDialog::editProfile()
{
	QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
	if(items.size() > 0)
	{
		Profile profile;
		profile.load(items[0]->text());
		if(!profile.camerasSufficant(cameras_->getCameras()))
		{
			QMessageBox::critical(this, "Profile Incompatible", "Not all cameras need for this profile are available");
			return;
		}
		EditProfileDialog dialog(cameras_, profile, this);
		dialog.show();
		int ret = dialog.exec();
		if(ret == QDialog::Accepted)
		{
			dialog.getProfile().store();
			ui->listWidget->clear();
			ui->listWidget->addItems(Profile::avaiableProfiles());
		}
	}
}

void ProfileDialog::deleteProfile()
{
	QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
	if(items.size() > 0)
	{
		Profile profile(items[0]->text());
		profile.deleteProfile();
		ui->listWidget->clear();
		ui->listWidget->addItems(Profile::avaiableProfiles());
	}
}

void ProfileDialog::addProfile()
{
	if(cameras_->getCameras().size() == 0)
	{
		QMessageBox::critical(this, "No Cameras", "At least one camera is needed to create a profile");
		return;
	}

	EditProfileDialog dialog(cameras_);
	dialog.show();
	int ret = dialog.exec();
	if(ret == QDialog::Accepted)
	{
		dialog.getProfile().store();
		ui->listWidget->clear();
		ui->listWidget->addItems(Profile::avaiableProfiles());
	}
}


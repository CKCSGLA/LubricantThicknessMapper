/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PROFILE_H
#define PROFILE_H
#include <QSettings>
#include <stdint.h>
#include <vector>
#include <QRandomGenerator>
#include <opencv2/core/mat.hpp>
#include <uvoscam.h>
#include <uvosunwrap/unwrap.h>

#include "camera.h"

class CameraSetup
{
public:
	size_t id;
	RemapMap remapMap;
	cv::Mat darkmap;
	cv::Mat bgmask;
	cam::Camera::BayerMode bayerMode = cam::Camera::BAYER_BLUE;
	double gain = 1.0;
	void store(const QString& filename) const;
	void load(const QString& name, size_t cameraId);
};

class LightingSetup
{
	static constexpr const char* GROUP = "Lighting";

public:
	double brightness = 0.25;
	uint8_t mask = 0;

	void store(QSettings& settings) const;
	void load(const QSettings& settings);
};

class Profile
{
	static constexpr const char* GROUP = "Profile";

	QString name_;

public:
	uint64_t id;
	LightingSetup lighting;
	double exposureTime = 1.0/60.0;
	cv::Mat lightmap;
	cv::Mat calcurve;
	std::vector<CameraSetup> cameras;
	bool nodistort = false;
	float kFactor = 0;

	Profile(const QString& name = "");
	void store(QSettings& settings) const;
	void store(const QString& name);
	void store();
	void load(QSettings& settings);
	void load(const QString& name);
	void load();
	void deleteProfile() const;
	void setName(const QString name){name_=name;}
	void setCamerasSetupsFromDescription(const std::vector<cam::Camera::Description>& desc);
	QString getName() const {return name_;}
	bool camerasSufficant(const std::vector<cam::Camera::Description>& desc) const;
	static QList<QString> avaiableProfiles();
	static QString profileLocation();
};

#endif // PROFILE_H

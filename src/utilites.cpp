/*UVOS*/

/* This file is part of MAClient copyright © 2021 Carl Philipp Klemm.
 *
 * MAClient is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (GPL) version 
 * 3 as published by the Free Software Foundation.
 *
 * MAClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAClient.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "utilites.h"

#include <vector>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QStringList>
#include <QMessageBox>
#include <thread>
#include <stdexcept>

bool saveToCsv(const QString& filename, const std::vector<double> &keys, const std::vector<double> &values, const QString& keyLable, const QString& valueLable )
{
	if(keys.size() != values.size())
	{
		throw std::invalid_argument("keys and values need to be the same size");
		return false;
	}
	QFile file(filename);
	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream fileStream(&file);
		fileStream<<"id"<<','<<keyLable<<','<<valueLable<<'\n';
		for(size_t i = 0; i < keys.size(); i++)
		{
			fileStream<<i<<','<<(int64_t)keys[i]<<','<<values[i]<<'\n';
		}
		fileStream.flush();
		file.close();
		return true;
	}
	return false;
}

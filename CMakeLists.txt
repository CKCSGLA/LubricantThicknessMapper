cmake_minimum_required(VERSION 3.5)

project(MAClient LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets Concurrent PrintSupport REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets Concurrent PrintSupport REQUIRED)
find_package(OpenCV REQUIRED)

set(PROJECT_SOURCES
	res/resources.qrc
	src/main.cpp
	src/cameras.cpp
	src/cameras.h
	src/camera.cpp
	src/camera.h
	src/profile.cpp
	src/profile.h
	src/imagepipeline.cpp
	src/imagepipeline.h
	src/ui/cameradialog.cpp
	src/ui/cameradialog.h
	src/ui/cameradialog.ui
	src/ui/profiledialog.cpp
	src/ui/profiledialog.h
	src/ui/profiledialog.ui
	src/ui/editprofiledialog.h
	src/ui/editprofiledialog.cpp
	src/ui/editprofiledialog.ui
	src/ui/mainwindow.cpp
	src/ui/mainwindow.h
	src/ui/mainwindow.ui
	src/ui/cvimageviewer.cpp
	src/ui/cvimageviewer.h
	src/ui/led.cpp
	src/ui/led.h
	src/ui/cameralistwidget.h
	src/ui/cameralistwidget.cpp
	src/ui/configurecameradialog.cpp
	src/ui/configurecameradialog.h
	src/ui/configurecameradialog.ui
	src/ui/aboutdiag.h
	src/ui/aboutdiag.cpp
	src/ui/aboutdiag.ui
	src/ui/statisticsdialog.h
	src/ui/statisticsdialog.cpp
	src/ui/statisticsdialog.ui
	src/ui/plot.cpp
	src/ui/plot.h
	src/qcustomplot/qcustomplot.h
	src/qcustomplot/qcustomplot.cpp
	src/regessioncalculator.h
	src/regessioncalculator.cpp
	src/ui/regressiondiag.h
	src/ui/regressiondiag.cpp
	src/ui/regressiondiag.ui
	src/utilites.h
	src/utilites.cpp
)


add_executable(MAClient ${PROJECT_SOURCES})
target_compile_options(MAClient PRIVATE "-std=gnu++17" "-Wall" "-O2" "-fno-strict-aliasing")
target_link_libraries(MAClient PRIVATE Qt${QT_VERSION_MAJOR}::Widgets Qt${QT_VERSION_MAJOR}::Concurrent Qt${QT_VERSION_MAJOR}::PrintSupport ${OpenCV_LIBS} -luvoscam -luvosled -luvosunwrap)
target_include_directories(${PROJECT_NAME} PRIVATE ${OpenCV_INCLUDE_DIRS} src src/ui)

set(CMAKE_INSTALL_PREFIX "/usr")
install(FILES res/icon.png DESTINATION /usr/share/icons RENAME MAClient.png)
install(FILES MAClient.desktop DESTINATION /usr/share/applications)
install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION bin)
